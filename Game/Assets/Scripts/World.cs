using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public GameObject tilePrefab;

    [Header("Settings")]
    public int levelWidth = 4;
    public int levelHeight = 5;

    public float tileOffset = 0.2f;

    [Header("Colors")]
    public Color tileColor;
    public Color backgroundColor; 


    private GameObject[,] tiles;
    private WorldGenerator worldGenerator;

    void Start()
    {
        worldGenerator = new WorldGenerator(this);
        tiles = worldGenerator.GenerateTiles();
    }

}
