using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator
{   
    private World world;

    public WorldGenerator(World world) {
        this.world = world;
    }

    public GameObject[,] GenerateTiles() {
        var tiles = new GameObject[world.levelWidth, world.levelHeight];

        Vector3 tileScale = world.tilePrefab.transform.localScale;
        var tileSpriteRenderer = world.tilePrefab.GetComponent<SpriteRenderer>();
        tileSpriteRenderer.sortingLayerID = SortingLayerUtils.FrontId;
        tileSpriteRenderer.color = world.tileColor;

        Vector2 spriteScale = tileSpriteRenderer.size;
        Vector3 prefabScale = world.tilePrefab.transform.localScale;

        float tileWidth = spriteScale.x * prefabScale.x;
        float tileHeight = spriteScale.y * prefabScale.y;

        float xOffset = world.tileOffset + tileWidth;
        float yOffset = world.tileOffset + tileHeight;

        float fullLevelWidth = world.levelWidth * tileWidth + (world.levelWidth - 1) * world.tileOffset;
        Func<int, float> getFullLevelHeight = (int height) => {
            return height * tileHeight + (height - 1) * world.tileOffset;
        };

        float startPosX = -fullLevelWidth / 2.0f + tileWidth / 2.0f;
        float startPosY = getFullLevelHeight(world.levelHeight) / 2.0f - tileHeight / 2.0f;

        // create tiles
        for (int j = 0; j < world.levelHeight; ++j) {
            for (int i = 0; i < world.levelWidth; ++i) {
                Vector3 tilePosition = new Vector3(startPosX + xOffset * i, startPosY - yOffset * j, 0.0f);
                tiles[i, j] = GameObject.Instantiate(world.tilePrefab, tilePosition, Quaternion.identity);
            }
        }

        // create backgrounds
        Action<int, float, Vector3> createBackground = (int height, float heightTileOffset, Vector3 position) => {
            GameObject background = GameObject.Instantiate(world.tilePrefab, position, Quaternion.identity);

            var backgroundSpriteRenderer = background.GetComponent<SpriteRenderer>();
            backgroundSpriteRenderer.size = new Vector2(fullLevelWidth + world.tileOffset * 2.0f, getFullLevelHeight(height) + heightTileOffset * 2.0f);
            backgroundSpriteRenderer.sortingLayerID = SortingLayerUtils.BackId;
            backgroundSpriteRenderer.color = world.backgroundColor;
        };

        createBackground(world.levelHeight - 2, world.tileOffset, Vector3.zero);
        createBackground(1, world.tileOffset * 0.6f, new Vector3(0.0f, startPosY, 0.0f));
        createBackground(1, world.tileOffset * 0.6f, new Vector3(0.0f, -startPosY, 0.0f));

        return tiles;
    }

}
