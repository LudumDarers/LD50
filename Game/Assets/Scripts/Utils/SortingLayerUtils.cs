using UnityEngine;

public static class SortingLayerUtils {
    public static readonly string BackName = "Back";
    public static int BackId => SortingLayer.NameToID(BackName);

    public static readonly string MidName = "Mid";
    public static int MidId => SortingLayer.NameToID(MidName);

    public static readonly string FrontName = "Front";
    public static int FrontId => SortingLayer.NameToID(FrontName);
}